import data from "../../sample-data.json";
import Table from "../Table";
import './companyDetails.css'

const CompanyDetails = () => {
  return (
    <div className="container">
      <h2 className="company-name" data-testid="company-name">
        {data?.companyInfo?.companyName}
      </h2>
      <div className="company-info" data-testid="company-info">
        <div>
          <p>{data?.companyInfo?.companyMotto}</p>
        </div>
        <div>
          <p>Since ({new Date(data?.companyInfo?.companyEst).toLocaleDateString()})</p>
        </div>
      </div>
      <Table />
    </div>
  );
};

export default CompanyDetails;
