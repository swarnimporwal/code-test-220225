import React from 'react';
import './companyDetailsPopup.css'

const CompanyDetailsPopup = ({ isModalOpen, selectedEmployee, setIsModalOpen }) => {
    const closeModal = () => {
      setIsModalOpen(false);
    };
  
    return (
      isModalOpen && (
        <div className="modal-overlay">
          <div className="modal-content">
            <div className="modal-header">
              <h2>Employee Details</h2>
              <span className="close" onClick={closeModal}>
                <img src="../../cancel.png" alt="Avatar" className="avatar" />
              </span>
            </div>
            <div className="modal-body">
              <div className="left-side">
                <img
                  // src={selectedEmployee.avatar}
                  src="../../man.png"
                  alt="Avatar"
                  className="avatar-image"
                />
                <p>{selectedEmployee?.jobTitle}</p>
                <p>Age :- {selectedEmployee?.age}</p>
                <p>
                  DOJ :-{" "}
                  {new Date(selectedEmployee?.dateJoined).toLocaleDateString()}
                </p>
              </div>
              <div className="right-side">
                <h3 className="border-bottom">
                  {" "}
                  {selectedEmployee?.firstName} {selectedEmployee?.lastName}
                </h3>
                <p> {selectedEmployee?.bio}</p>
              </div>
            </div>
          </div>
        </div>
      )
    );
  };
  
  export default CompanyDetailsPopup;
  