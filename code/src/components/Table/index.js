import { useState } from "react";
import data from "../../sample-data.json";
import { renderPageNumbers, tableHeader } from "../../utils/helper";
import "./table.css"
import CompanyDetailsPopup from "../CompanyDetailsPopup";

const Table = () => {
  const itemsPerPage = 3;
  const [sortedColumn, setSortedColumn] = useState(null);
  const [sortOrder, setSortOrder] = useState("asc");
  const [currentPage, setCurrentPage] = useState(1);
  const [searchQuery, setSearchQuery] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedEmployee, setSelectedEmployee] = useState(null);

  const sortTable = (column) => {
    if (sortedColumn === column) {
      setSortOrder(sortOrder === "asc" ? "desc" : "asc");
    } else {
      setSortedColumn(column);
      setSortOrder("asc");
    }
  };

  const getSortArrow = (column) => {
    if (sortedColumn === column) {
      return sortOrder === "asc" ? "▲" : "▼";
    }
    return "";
  };

  const sortedEmployees = [...data?.employees].sort((a, b) => {
    const aValue = sortedColumn ? a[sortedColumn] : "";
    const bValue = sortedColumn ? b[sortedColumn] : "";

    if (!isNaN(aValue) && !isNaN(bValue)) {
      return sortOrder === "asc" ? aValue - bValue : bValue - aValue;
    } else {
      return sortOrder === "asc"
        ? aValue.localeCompare(bValue)
        : bValue.localeCompare(aValue);
    }
  });

  const filteredAndSortedData = sortedEmployees.filter((employee) => {
    return (
      employee.id.toLowerCase().includes(searchQuery.toLowerCase()) ||
      employee.firstName.toLowerCase().includes(searchQuery.toLowerCase()) ||
      employee.lastName.toLowerCase().includes(searchQuery.toLowerCase()) ||
      employee.jobTitle.toLowerCase().includes(searchQuery.toLowerCase()) ||
      employee.contactNo.includes(searchQuery) ||
      employee.address.toLowerCase().includes(searchQuery.toLowerCase()) ||
      employee.age.toString().includes(searchQuery) ||
      employee.bio.toLowerCase().includes(searchQuery.toLowerCase()) ||
      employee.dateJoined.includes(searchQuery)
    );
  });

  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = filteredAndSortedData?.slice(
    indexOfFirstItem,
    indexOfLastItem
  );

  const totalPages = Math.ceil(filteredAndSortedData?.length / itemsPerPage);
  return (
    <div className="table-container" data-testid="table-id">
      <div className="search-container">
        <input
          type="text"
          className="search-input"
          value={searchQuery}
          onChange={(e) => {
            setSearchQuery(e.target.value);
            setCurrentPage(1);
          }}
          placeholder="Search..."
        />
      </div>
      <table>
        <thead>
          <tr>
            {tableHeader?.map((ele, index) => {
              return (
                <th
                  key={index}
                  className={ele?.class}
                  onClick={() => sortTable(ele.name)}
                >
                  {ele?.label}{" "}
                  {getSortArrow(ele.name) === "" ? (
                    <img className="arrow-css" src="../../up-down.png" alt="sort-arrow" />
                  ) : (
                    getSortArrow(ele.name)
                  )}
                </th>
              );
            })}
            <th>View</th>
          </tr>
        </thead>
        <tbody>
          {currentItems?.map((employee, index) => (
            <tr key={index}>
              <td>{employee.id}</td>
              <td>
                <div className="name-column">
                  <img
                    // src={employee.avatar}
                    src="../../profile.png"
                    alt="Avatar"
                    className="avatar"
                  />
                  {employee.firstName} {employee.lastName}
                </div>
              </td>
              <td>{employee.jobTitle}</td>
              <td>{employee.contactNo}</td>
              <td>{employee.address}</td>
              <td>{employee.age}</td>
              <td>{employee.bio}</td>
              <td>{new Date(employee.dateJoined).toLocaleDateString()}</td>
              <td className="text-center">
                <img
                  src="../../eye.png"
                  alt="Avatar"
                  className="avatar cursor-pointer"
                  onClick={() => {
                    setSelectedEmployee(employee);
                    setIsModalOpen(true);
                  }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="pagination">
        <ul>{renderPageNumbers(currentPage, setCurrentPage, totalPages)}</ul>
      </div>

      {/* Employee Detail Modal */}
      <CompanyDetailsPopup
        isModalOpen={isModalOpen}
        selectedEmployee={selectedEmployee}
        setIsModalOpen={setIsModalOpen}
      />
    </div>
  );
};

export default Table;
