import CompanyDetails from "./components/CompanyDetails";
import "./app.css"
function App() {
  return (
    <div className="App">
      <CompanyDetails />
    </div>
  );
}

export default App;
