import { render, screen } from '@testing-library/react';
import CompanyDetails from '../components/CompanyDetails';

describe('CompanyDetails Component', () => {
  it('renders company details', () => {
    render(<CompanyDetails />);
  
    const companyNameElement = screen.getByTestId('company-name');
    expect(companyNameElement).toBeInTheDocument();

    const companyMottoElement = screen.getByTestId('company-info');
    expect(companyMottoElement).toBeInTheDocument();
  });

  it('renders table with data', () => {
    render(<CompanyDetails />);

    const tableElement = screen.getByTestId('table-id');
    expect(tableElement).toBeInTheDocument();
  });
});
