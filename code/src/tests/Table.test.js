import { render, screen, fireEvent } from "@testing-library/react";
import Table from "../components/Table";

test("renders table with data", () => {
  render(<Table />);

  const tableElement = screen.getByTestId("table-id");
  expect(tableElement).toBeInTheDocument();

  const searchInput = screen.getByPlaceholderText("Search...");
  expect(searchInput).toBeInTheDocument();

  const tableHeaders = screen.getAllByRole("columnheader");
  expect(tableHeaders.length).toBeGreaterThan(0);

  const tableRows = screen.getAllByRole("row");
  expect(tableRows.length).toBeGreaterThan(1);
});

test("allows searching for data", () => {
  render(<Table />);

  const searchInput = screen.getByPlaceholderText("Search...");

  fireEvent.change(searchInput, {
    target: { value: "Legacy Infrastructure Coordinator" },
  });

  const searchedRow = screen.getByText("Legacy Infrastructure Coordinator");
  expect(searchedRow).toBeInTheDocument();
});

test("sorting works for different columns", () => {
  render(<Table />);

  const columnName = "Name";
  const columnHeader = screen.getByText(columnName);
  fireEvent.click(columnHeader);

  const sortedColumnHeader = screen.getByText(`${columnName} ▲`);
  expect(sortedColumnHeader).toBeInTheDocument();
});

test("clicking on table headers triggers sortTable function", () => {
  render(<Table />);

  const columnName = "Name";
  const columnHeader = screen.getByText(columnName);
  fireEvent.click(columnHeader);

  expect(screen.getByText("Name ▲")).toBeInTheDocument();
});

test("table displays all data when search input is empty", () => {
  render(<Table />);

  const searchInput = screen.getByPlaceholderText("Search...");

  const allDataRows = screen.getAllByRole("row").slice(1); // Exclude header row
  expect(allDataRows.length).toBeGreaterThan(1);
});

test("search is case-insensitive", () => {
  render(<Table />);

  const searchInput = screen.getByPlaceholderText("Search...");

  fireEvent.change(searchInput, {
    target: { value: "legacy infrastructure coordinator" }, 
  });

  const searchedRow = screen.getByText(/Legacy Infrastructure Coordinator/i);
  expect(searchedRow).toBeInTheDocument();
});

test("search works across multiple fields", () => {
  render(<Table />);

  const searchInput = screen.getByPlaceholderText("Search...");

  fireEvent.change(searchInput, {
    target: { value: "0484 725 380" }, 
  });

  const searchedRow = screen.getByText(/0484 725 380/i);
  expect(searchedRow).toBeInTheDocument();
});

test("clicking on page numbers changes currentPage state and updates displayed data", () => {
  render(<Table />);

  const page2Button = screen.getByText("2"); 
  fireEvent.click(page2Button);

  expect(screen.getByText("2")).toHaveClass("active");
});

test("first button to be disabled when not applicable", () => {
  render(<Table />);

  expect(screen.getByText("Prev")).toHaveClass("disabled");
});