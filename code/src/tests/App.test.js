import { render, screen } from '@testing-library/react';
import App from '../App';

test('renders company details', () => {
  render(<App />);
  
  const companyNameElement = screen.getByTestId('company-name');
  expect(companyNameElement).toBeInTheDocument();

  const companyInfoElement = screen.getByTestId('company-info');
  expect(companyInfoElement).toBeInTheDocument();

});
