import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import CompanyDetailsPopup from "../components/CompanyDetailsPopup";

test("renders CompanyDetailsPopup with employee details", () => {
  const selectedEmployee = {
    firstName: "John",
    lastName: "Doe",
    jobTitle: "Software Engineer",
    age: 30,
    dateJoined: new Date(),
    bio: "A software engineer with a passion for coding.",
  };

  render(
    <CompanyDetailsPopup
      isModalOpen={true}
      selectedEmployee={selectedEmployee}
      setIsModalOpen={() => {}}
    />
  );

  const employeeDetailsHeader = screen.getByText("Employee Details");
  expect(employeeDetailsHeader).toBeInTheDocument();

  const employeeName = screen.getByText("John Doe");
  expect(employeeName).toBeInTheDocument();

  const employeeJobTitle = screen.getByText("Software Engineer");
  expect(employeeJobTitle).toBeInTheDocument();

  const employeeAge = screen.getByText("Age :- 30");
  expect(employeeAge).toBeInTheDocument();

  const employeeBio = screen.getByText(
    "A software engineer with a passion for coding."
  );
  expect(employeeBio).toBeInTheDocument();
});

test("closes CompanyDetailsPopup on clicking close button", () => {
  const setIsModalOpenMock = jest.fn();
  render(
    <CompanyDetailsPopup
      isModalOpen={true}
      selectedEmployee={{}}
      setIsModalOpen={setIsModalOpenMock}
    />
  );
  fireEvent.click(
    screen
      .getByText("Employee Details")
      .closest(".modal-header")
      .querySelector(".close")
  );
  expect(setIsModalOpenMock).toHaveBeenCalledWith(false);
});
