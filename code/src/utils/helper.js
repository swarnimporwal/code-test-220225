export const tableHeader = [
  {
    name: "id",
    label: "ID",
    class: "",
  },
  {
    name: "firstName",
    label: "Name",
    class: "",
  },
  {
    name: "jobTitle",
    label: "Job Title",
    class: "width-120",
  },
  {
    name: "contactNo",
    label: "Contact No",
    class: "width-120",
  },
  {
    name: "address",
    label: "Address",
    class: "",
  },
  {
    name: "age",
    label: "Age",
    class: "",
  },
  {
    name: "bio",
    label: "Bio",
    class: "",
  },
  {
    name: "dateJoined",
    label: "Date Joined",
    class: "width-120",
  },
];

export const renderPageNumbers = (currentPage, setCurrentPage, totalPages) => {
  const pageNumbers = [];
  pageNumbers.push(
    <li
      key="prev"
      onClick={() => setCurrentPage(currentPage > 1 ? currentPage - 1 : 1)}
      className={currentPage === 1 ? "disabled" : ""}
    >
      Prev
    </li>
  );
  for (let i = 1; i <= totalPages; i++) {
    pageNumbers.push(
      <li
        key={i}
        onClick={() => setCurrentPage(i)}
        className={currentPage === i ? "active" : ""}
      >
        {i}
      </li>
    );
  }
  pageNumbers.push(
    <li
      key="next"
      onClick={() =>
        setCurrentPage(currentPage < totalPages ? currentPage + 1 : totalPages)
      }
      className={currentPage === totalPages ? "disabled" : ""}
    >
      Next
    </li>
  );
  return pageNumbers;
};
